<?php

namespace CommsExpress\Sage;


interface ExporterContract
{
    function createExport(...$data) : bool;

    function createArrayOfExports(array $array) : bool;

    function run();

    function getExports();
}