<?php

namespace CommsExpress\Sage;


use CommsExpress\Sage\Models\SageExport;

class Manager
{
    public function export($exporter)
    {
        $export_entities = $this->getExportEntitiesFromExporter($exporter);

        $request = $this->buildRequestFromExporter($exporter);

        $request->run();

        if($request instanceof BatchRequest)
        {
            foreach($request->getResponse()->getBody() as $response)
            {
                $entity = SageExport::find($response->reference);
                $entity->setStatusToSent();
                $entity->save();
            }

        }
        else
        {
            $response = $request->getResponse()->getBody();

            $entity = $export_entities[0];
            $entity->setStatusToSent();
            $entity->sage_interface_import_id = $response->data->id;
            $entity->save();
        }

        return true;
    }

    public function buildRequest($endpoint, $type = 'get', $body = null)
    {
        //  If the endpoint is an array, we will assume it is an array of requests
        //  of varying endpoints, types and bodies that are to be sent together in a
        //  batch request.
        if(is_array($endpoint))
        {
            return $this->buildBatchRequest($endpoint);
        }

        //  If the body is not null and is not an associative array and not to be a batch request
        //  we will assume we want to make multiple requests to the same endpoint of the same type.
        //  Only the body would differ. This is purely a convenience method.
        if(!is_null($body) && is_array($body) && is_numeric(key($body)) && $endpoint != 'batch')
        {
            $requests = [];

            foreach($body as $id => $item)
            {
                $requests[$id] = [
                    $endpoint,
                    $type,
                    $item
                ];
            }

            return $this->buildBatchRequest($requests);
        }

        return new Request($endpoint, $type, $body);
    }

    private function getExportEntitiesFromExporter($exporter)
    {
        if(is_array($exporter))
        {
            return $this->getExportEntitiesFromArrayOfExporters($exporter);
        }

        return $exporter->getExports();
    }

    private function getExportEntitiesFromArrayOfExporters($exporters)
    {
        $entities = [];

        foreach($exporters as $exporter)
        {
            array_merge($entities, $exporter->getExports());
        }

        return $entities;
    }

    private function buildBatchRequest(array $requests)
    {
        $body = [];

        foreach($requests as $id => $request)
        {
            $body[$id] = $this->buildRequest(...$request);
        }

        return new BatchRequest($body);
    }

    private function buildRequestFromExporter($exporter)
    {
        if(is_array($exporter))
        {
            $this->buildRequestFromArrayOfExporters($exporter);
        }

        $body = [];

        foreach($exporter->getExports() as $model)
        {
            $body[$model->id] = $model->body;
        }

        return $this->buildRequest($exporter->getEndpoint(), 'post', sizeof($body) > 1 ? $body : array_first($body));
    }

    private function buildRequestFromArrayOfExporters(array $exporters)
    {
        foreach($exporters as $exporter)
        {
            $request = $this->buildRequestFromExporter($exporter);
        }

        //BUILD BATCH REQUEST
    }

    private function updateExportEntitiesToSent($export_entities)
    {
        foreach($export_entities as $entity)
        {
            $entity->status = 'Sent';
            $entity->save();
        }
    }
}