<?php

namespace CommsExpress\Sage;

use CommsExpress\Sage\Models\SageExport;
use CommsExpress\Sage\Traits\InteractsWithSage;
use Illuminate\Http\Resources\Json\Resource;

abstract class Exporter implements ExporterContract
{
    use InteractsWithSage;

    protected $endpoint;

    protected $mapper;

    protected $exports = [];

    protected $request;

    public function __construct(...$parameters)
    {
        if(!empty($parameters))
        {
            $this->createExport(...$parameters);
        }
    }

    abstract function getResourceClass();

    public function createExport(...$parameters) : bool
    {
        if(is_array($parameters[0]) && is_numeric(key($parameters[0])))
        {
            return $this->createArrayOfExports($parameters[0]);
        }

        $model = SageExport::create([
            'endpoint'  =>  $this->getEndpoint(),
            'body'      =>  json_encode($this->buildResource(...$parameters)),
            'status'    =>  'Created'
        ]);

        foreach($parameters as $param)
        {
            foreach($this->reference_listener as $class)
            {
                if(is_a($param, $class))
                {
                    $param->sageExports()->attach($model);
                }
            }
        }

        $this->exports[] = $model;

        return true;
    }

    public function createArrayOfExports(array $array) : bool
    {
        foreach($array as $data)
        {
            if(is_array($data))
            {
                $this->createExport(...$data);
            }
            else
            {
                $this->createExport($data);
            }
        }

        return true;
    }

    public function run()
    {
        $body = [];

        foreach($this->exports as $export)
        {
            $body[] = $export->body;
        }

        $request = $this->buildRequest($this->getEndpoint(), 'post', $body);

        $request->run();

        return $request;
    }

    public function getExports()
    {
        return $this->exports;
    }

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    protected function buildResource(): Resource
    {
        return $this->getResourceClass()::make(...func_get_args());
    }
}