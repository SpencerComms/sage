<?php

namespace CommsExpress\Sage;

class BatchRequest extends Request
{
    public function __construct(array $requests = [])
    {
        parent::__construct('batch', 'post', []);

        $this->addArrayOfRequests($requests);
    }

    public function addRequest(Request $request, $reference = null)
    {
        $body = $this->getBody();

        if($reference)
        {
            $body[$reference] = [
                'endpoint'  =>  $request->getEndpoint(),
                'type'      =>  $request->getType(),
                'body'      =>  $request->getBody()
            ];
        }
        else
        {
            $body[] = [
                'endpoint'  =>  $request->getEndpoint(),
                'type'      =>  $request->getType(),
                'body'      =>  $request->getBody()
            ];
        }

        $this->setBody($body);
    }

    public function addArrayOfRequests(array $requests)
    {
        foreach($requests as $id => $request)
        {
            $this->addRequest($request, $id);
        }

        return true;
    }
}