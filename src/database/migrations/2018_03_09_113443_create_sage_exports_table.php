<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSageExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sage_exports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('endpoint');
            $table->integer('sage_interface_import_id')->nullable();
            $table->json('body');
            $table->string('status');
            $table->timestamps();
        });

        Schema::create('sage_export_reference', function (Blueprint $table){
            $table->integer('sage_export_id')->unsigned();
            $table->morphs('reference');
            $table->json('parameters')->nullable();

            $table->foreign('sage_export_id')
                ->references('id')
                ->on('sage_exports')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sage_export_reference');
        Schema::dropIfExists('sage_exports');
    }
}
