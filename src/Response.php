<?php

namespace CommsExpress\Sage;


class Response
{
    private $status;

    private $body;

    public function __construct($status, $body)
    {
        $this->status = $status;
        $this->setBody($body);
    }

    private function setBody($body)
    {
        if(is_array($body))
        {
            return $this->setArrayOfBodies($body);
        }

        $this->body = json_decode($body);
    }

    private function setArrayOfBodies($bodies)
    {
        $this->body = [];

        foreach($bodies as $b)
        {
            $this->body[$b->reference] = new Response($b->status_code, $b->body);
        }
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getBody()
    {
        return $this->body;
    }
}