<?php

namespace CommsExpress\Sage;


use GuzzleHttp\Client;

class Request
{
    private $valid_endpoints = [
        'batch',
        'stockitem/import',
        'stockitem_supplier'
    ];

    private $endpoint;

    private $type;

    private $body;

    private $response;

    public function __construct($endpoint = null, $type = null, $body = null)
    {
        $this->setEndpoint($endpoint);
        $this->setType($type);
        $this->setBody($body);
        $this->setClient();
    }

    private function setClient()
    {
        //We will use the GuzzleHTTP client
        $this->client = new Client(['base_uri' => 'http://sage-interface.test/api/']);
    }

    public function setEndpoint($value)
    {
        $this->validateEndpoint($value);

        $this->endpoint = $value;
    }

    public function getEndpoint()
    {
        return $this->endpoint;
    }

    public function setType($value)
    {
        $this->type = $value;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setBody($array)
    {
        $this->body = !is_array($array) ? ($this->isJson($array) ? json_decode($array, true) : $array) : $array;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function run()
    {
        $options = [];

        if($this->getType() == 'post')
        {
            $options['form_params'] = $this->getBody();
        }

        $response = $this->client->request($this->getType(), $this->getEndpoint(), $options);

        $this->response = new Response($response->getStatusCode(), $response->getBody()->getContents());
    }

    private function validateEndpoint($endpoint)
    {
        if(!in_array($endpoint, $this->valid_endpoints))
        {
            throw new Exception('Invalid Sage Endpoint');
        }
    }

    private function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}