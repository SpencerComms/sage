<?php

namespace CommsExpress\Sage\Traits;


use CommsExpress\Sage\Manager;

trait InteractsWithSage
{
    public function __construct()
    {
        $this->sage = app()->make(Manager::class);
    }
}