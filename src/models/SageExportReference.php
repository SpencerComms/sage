<?php

namespace CommsExpress\Sage\Models;

use Illuminate\Database\Eloquent\Relations\MorphPivot;

class SageExportReference extends MorphPivot
{
    public function model()
    {
        return $this->morphTo('reference');
    }
}