<?php

namespace CommsExpress\Sage\Models;

use Illuminate\Database\Eloquent\Model;

class SageExport extends Model
{
    protected $table = 'sage_exports';

    public $incrementing = true;

    public $timestamps = true;

    protected $guarded = [];

    public function references()
    {
        return $this->hasMany(SageExportReference::class);
    }

    public function setStatusToSent()
    {
        $this->status = 'Sent';
    }
}