<?php

namespace CommsExpress\Sage\Models;


interface Exportable
{
    public function sageExports();
}