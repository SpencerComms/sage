<?php

namespace CommsExpress\Sage\Traits;

use CommsExpress\Sage\Models\SageExport;
use CommsExpress\Sage\Models\SageExportReference;

trait ExportsToSage
{
    public function sageExports()
    {
        return $this->morphToMany(SageExport::class, 'reference', 'sage_export_reference')->withPivot('parameters')->using(SageExportReference::class);
    }
}